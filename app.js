const express = require('express');
const app = express()
const port = process.env.PORT
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}
// const port = 3000


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.set('views', 'views');
app.set('view engine', 'ejs');
app.use(express.static('views'));


app.listen(port, () => {
    console.log(`Running on localhost: ${port}`)
})

app.use('/', (req, res) => {
    res.status(200).render('home');
})

